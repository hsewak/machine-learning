#(a) Build a classifier that classifies sequences into one of the 14 activities provided.
#To make features, you should vector quantize, then use a histogram of cluster centers 
#(as described in the subsection; this gives a pretty explicit set of steps to follow). 
#You will find it helpful to use hierarchical k-means to vector quantize. 
#You may use whatever multi-class classifier you wish, though I'd start with R's decision forest, because it's easy to use and effective.
#You should report (a) the total error rate and (b) the class confusion matrix of your classifier.

setwd("C:/Users/hsewak/Desktop/Urbana/MachineLearning/Wk5/")
library(randomForest)
library(cluster)
library(caret)

set.seed(1234)

create.indices = function (data) {
  indices = data.frame(matrix(rep(0, times = length(data) * 2), nrow = length(data), ncol = 2))
  names(indices) = c("start", "stop")
  return(indices)
}

train.indices = function (data) {
  indices = create.indices(data)
  for (i in 1:length(data)) {
    indices[i, 1] = 1
    indices[i, 2] = floor(length(data[[i]]) * 0.8)
  }
  return(indices)
}

test.indices = function (train.indices, data) {
  indices = create.indices(data)
  for (i in 1:length(data)) {
    indices[i, 1] = train.indices[i, 2] + 1
    indices[i, 2] = length(data[[i]])
  }
  return(indices)
}

#nearest cluster
nc.segment = function (segment, centers, seg.size, dict.size) {
  which.min(apply((matrix(rep(segment, times = dict.size), ncol = 3 * seg.size, nrow = dict.size, byrow = TRUE) - centers)^2, 
                  MARGIN = 1, 
                  FUN = sum))
}

create.hist = function (set, dictionary, seg.size, dict.size) {
  sapply(set, FUN = function (category) {
    sapply(category, FUN = function (sample) {
      table(factor(lapply(sample, FUN = function (segment) nc.segment(segment, dictionary, seg.size, dict.size)), levels = 1:dict.size)) / length(sample)
    })
  })
}

create.factor = function (set) {
  factor(unlist(lapply(1:length(set), FUN = function (category_index) {
    rep(cat.name[category_index], times = length(set[[category_index]]))
  })), cat.name)
}

create.seg = function (file_contents, seg.size) {
  lapply(file_contents, FUN = function (category) {
    lapply(category, FUN = function (category_file.contents) {
      seg.count = floor(nrow(category_file.contents) / seg.size)
      
      lapply(lapply(0:(seg.count - 1),
                    FUN = function (i) category_file.contents[((i * seg.size) + 1):((i + 1) * seg.size), ]),
             FUN = function (segment) c(segment$V1, segment$V2, segment$V3))
    })
  })
}

run = function (file_contents, seg.size, dict.size) {
  data = create.seg(file_contents, seg.size)
  
  train.indices = train.indices(data)
  testing.indices = test.indices(train.indices, data)
  
  training = lapply(1:length(data), FUN = function (i) data[[i]][train.indices[i,1]:train.indices[i,2]])
  testing = lapply(1:length(data), FUN = function (i) data[[i]][testing.indices[i,1]:testing.indices[i,2]])
  
  
  training.data.flattened = matrix(unlist(training), ncol = 3 * seg.size, byrow = TRUE)
  
  dictionary = kmeans(training.data.flattened, dict.size, iter.max = 20)$centers
  
  training.histogram = create.hist(training, dictionary, seg.size, dict.size)
  training.classes = create.factor(training)
  training.histogram = as.data.frame(matrix(unlist(training.histogram), ncol = dict.size, byrow = TRUE))
  
  testing.histogram = create.hist(testing, dictionary, seg.size, dict.size)
  testing.classes = create.factor(testing)
  testing.histogram = as.data.frame(matrix(unlist(testing.histogram), ncol = dict.size, byrow = TRUE))
  
  forest = randomForest(training.histogram, y = training.classes)
  testing.predicted = predict(forest, testing.histogram)
  
  cm = confusionMatrix(testing.predicted, testing.classes)
}


data.folder = "HMP_Dataset"
data.directories = list.dirs(path="HMP_Dataset", full.names = FALSE, recursive = FALSE)

data.contents = lapply(data.directories, FUN = function (category_directory) {
  category_directory.full_path = paste("HMP_Dataset", category_directory, sep = "/")
  
  cat.file = list.files(path = category_directory.full_path)
  
  return(lapply(cat.file, FUN= function (category_file) {
    category_file.full_path = paste(category_directory.full_path, category_file, sep = "/")
    
    return(as.data.frame(read.table(category_file.full_path), colnames = c("x", "y", "z")))
  }))
})

names(data.contents) = data.directories
cat.name = data.directories

rm(data.directories)
rm(data.folder)

run.accuracy = function (seg.size, dict.size) {
  cm = run(data.contents, seg.size, dict.size)
  print(paste(
    "Segment size:", seg.size, 
    "Dictionary size:", dict.size, 
    "Accuracy:", cm$overall[1]))
  print("Confusion matrix")
  print(cm$table)
  write.csv(cm$table, 'cm.csv')
}

run.accuracy(32, 480)

seg.size = c(8, 12, 16, 32)
dict.size = c(14, 28, 56, 120, 240, 320, 480)

try.combo = matrix(unlist(lapply(seg.size,FUN = function (seg.size) lapply(dict.size, FUN = function (dict.size) c(seg.size, dict.size)))),
                      nrow = length(seg.size) * length(dict.size),
                      ncol = 2,
                      byrow = TRUE)

accuracies = matrix(apply(try.combo, MARGIN = 1, FUN = function (try.combo) run(data.contents, try.combo[1], try.combo[2])$overall[1]),
                     nrow = length(seg.size),
                     ncol = length(dict.size),
                     byrow = TRUE,
                     dimnames = list(seg.size, dict.size))

print(accuracies)
write.csv(accuracies, 'accuracy.csv')

