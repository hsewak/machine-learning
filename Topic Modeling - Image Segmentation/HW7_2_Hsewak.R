setwd("C:/Users/hsewak/Desktop/Urbana/MachineLearning/Wk10/")

library(jpeg)
library(grid)
library(gridExtra)
library(imager)
library(png)

img1 = readJPEG("RobertMixed03.jpg")
img2 = readJPEG("smallstrelitzia.jpg")
img3 = readJPEG("smallsunset.jpg")

dim1 = dim(img1)[1:2]
dim2 = dim(img2)[1:2]
dim3 = dim(img3)[1:2]

disp.img = function(img, dim) {
  len = length(img) / 3
 
  r = img[1:len]
  g = img[(len+1):(2*len)]
  b = img[(2*len+1):(3*len)]
  
  img.matrix = rgb(r, g, b)
  dim(img.matrix) = dim
  img.matrix = t(img.matrix)
  grid.raster(img.matrix, interpolate = F)
}

segment = function(seg, img, dim) {
  img.sd = sd(img)
  x = as.vector(img / img.sd)
  km = kmeans(x, centers = seg)
  centers = km$centers
  
  # prepare values to be used in e and m steps
  m = length(x)
  n = seg
  w = matrix(0, nrow = m, ncol = n)
  pi = as.vector(rep(1/n, n))
  mu = as.vector(centers)
  epsilon = 0.8
  
  iter = 0
  flag = TRUE
  while(flag) {
    mu.old = mu
    for(i in 1:m) {
      
      den = 0
      for(j in 1:n) {
        sum.j = sum(exp((-1/2 * (x[i] - mu[j])^2)) * pi[j])
        den = den + sum.j
      }
      
      for(j in 1:n) {
        w[i, j] = (exp((-1/2 * (x[i] - mu[j])^2)) * pi[j]) / den
      }
    }
   
    for(j in 1:n) {
      mu[j] = sum(x * w[, j]) / sum(w[, j])
      pi[j] = sum(w[, j]) / m
    } 
    if(abs(sum(mu) - sum(mu.old)) <  epsilon) {
      flag = FALSE
    }
    
    iter = iter + 1
    print(iter)
  }

  indices = apply(w, 1, which.max)
  
  clusters.means = as.vector(rep(0, n))
  for(j in 1:n) {
    clusters.means[j] = mean(img[indices == j])
  }

  segmented = clusters.means[indices]
  disp.img(segmented, dim) 
}

segment(10, img1, dim1)
segment(20, img1, dim1)
segment(50, img1, dim1)

segment(10, img2, dim2)
segment(20, img2, dim2)
segment(50, img2, dim2)

segment(10, img3, dim3)
segment(20, img3, dim3)
segment(50, img3, dim3)

set.seed(-129)
segment(20, img3, dim3)

set.seed(1522)
segment(20, img3, dim3)

set.seed(-8)
segment(20, img3, dim3)

set.seed(165)
segment(20, img3, dim3)

set.seed(336285)
segment(20, img3, dim3)